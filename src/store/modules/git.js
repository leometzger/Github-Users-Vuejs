import * as types from '../mutation-types';


// initial state
const state = {
    gitUsers: []
}

// getters
const getters = {
    users: () => state.gitUsers,
    usersCount: () => state.gitUsers.length,
    repositoriesCount: () => {
        let count = 0;
        state.gitUsers.forEach((user) => { count += user["public_repos"];}, this);
        return count;
    },
    followersCount: () => {
        let count = 0;
        state.gitUsers.forEach((user) => { count += user.followers }, this);
        return count;
    } 
}

// mutations
const mutations = {
    [types.GET_USER](state, user) {
        state.gitUsers.push(user);
    },
}

export default {
    state,
    getters,
    mutations
}