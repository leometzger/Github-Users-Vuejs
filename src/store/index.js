import Vue from 'vue';
import Vuex from 'vuex';
import Git from './modules/git';
import * as types from './mutation-types.js';
import getGithubUser from '../api/git';

Vue.use(Vuex);

// state
const state = {
    msg: 'Hello!',
    usernameSearch: ''
}

// mutations
const mutations = {
    [types.UPDATE_USERNAME_SEARCHED](state, value) {
        this.state.usernameSearch = value;
    },
    [types.SEARCH_CLEAR] (state, value) {
        this.state.usernameSearch = '';
    }
}

// actions
const actions = {
    reverse: ({ commit }) => commit('reverse'),

    getUser: async ({ commit, state }) => {
        const username = state.usernameSearch;
        getGithubUser(username).then(user => {
                commit(types.GET_USER, user);
                commit(types.SEARCH_CLEAR);
            }).catch((ex) => {
        });
    }
}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    modules: {
        Git
    }
})